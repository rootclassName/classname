import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
export default new Router({
  routes: [{
      path: "/",
      component: () => import("@/views/login")
    },
    {
      path: "/home",
      component: () => import("@/views/home"),
      children: [
        {
          path: "/home/addQuestions",
          component: () => import("@/views/addQuestions")
        },
        {
          path: "/home/checkQuestions",
          component: () => import("@/views/checkQuestions")
        },
        {
          path: "/home/classifyQuestions",
          component: () => import("@/views/classifyQuestions")
        },
        {
          path: "/home/classManagement",
          component: () => import("@/views/classManagement")
        },
        {
          path: "/home/roomManagement",
          component: () => import("@/views/roomManagement")
        },
        {
          path: "/home/studentManagement",
          component: () => import("@/views/studentManagement")
        },
        {
          path: "/home/adduser",
          component: () => import("@/views/adduser")
        },
        {
          path: "/home/usershow",
          component: () => import("@/views/usershow")
        },
        {
          path: "/home/addexam",
          component: () => import("@/views/addexam")
        },
        {
          path: "/home/examList",
          component: () => import("@/views/examList")
        },
        {
          path: "/home/waitClass",
          component: () => import("@/views/waitClass")
        },
        {
          path: "/detail",
          component: () => import("@/views/detail")
        }
      ]
    },

  ]
});
