import axios from "./index";

export const login = (user_name, user_pwd) => {
  return axios.post("/user/login", {
    user_name,
    user_pwd
  });
};
export const addExam = (questions_type_id, questions_stem,subject_id,exam_id,user_id,questions_answer,title) => {
  return axios.post("/exam/questions", {
    questions_type_id,
    questions_stem,
    subject_id,
    exam_id,
    user_id,
    questions_answer,
    title
  });
};
export const checkQuestions = () => {
  return axios.get("/exam/subject");
};
export const checkadd = () => {
  return axios.get("/exam/examType");
};
export const checktype = () => {
  return axios.get("/exam/getQuestionsType");
};
export const seacrhAdd = subject_id => {
  return axios.get("/exam/questions/condition", {
    params: {
      subject_id
    }
  });
};

export const allAdd = () => {
  return axios.get("/exam/questions/new");
};
export const classMange = () => {
  return axios.get("/manger/grade");
};
export const classNumber = () => {
  return axios.get("/manger/room");
};

export const student = () => {
  return axios.get("/manger/student");
};
export const addRoom = room_text => {
  return axios.post("/manger/room", {
    room_text
  });
};
export const deleteRoom = room_id => {
  return axios.delete("/manger/room/delete", {
    data: {
      room_id
    }
  });
};
export const insertQuestionsType = (text, id) => {
  return axios.post("/exam/insertQuestionsType", {
    text,
    id
  });
};

export const deleteClass = grade_id => {
  return axios.delete("/manger/grade/delete", {
    data: {
      grade_id
    }
  });
};
export const updataClass = (grade_id, grade_name, subject_id, room_id) => {
  return axios.put("/manger/grade/update", {
    grade_id,
    grade_name,
    subject_id,
    room_id
  });
};

export const addClass = (grade_name, room_id, subject_id) => {
  return axios.post('/manger/grade', {
    grade_name,
    room_id,
    subject_id
  })
}
export const delclass = (grade_id) => {
  return axios.delete('/manger/grade/delete', {
    grade_id
  })
}
export const user = (user_name, user_pwd, identity_id) => {
  return axios.post('/user', {
    user_name,
    user_pwd,
    identity_id
  })
}
export const identity = (identity_text) => {
  return axios.post('/user/identity/edit', {
    params: {
      identity_text
    }
  })
}
export const authority = () => {
  return axios.get('/user/identity_api_authority_relation')
}
export const userAuthority = () => {
  return axios.get('/user/user')
}
export const useridentity = () => {
  return axios.get('/user/identity')
}
export const view_authority = () => {
  return axios.get('/user/view_authority')
}
export const identity_view_authority_relation = () => {
  return axios.get('/user/identity_view_authority_relation')
}
export const examList = () => {
  return axios.get("/exam/exam");
};

export const detail = () => {
  return axios.get("/exam/exam/w5tcy-g2dts");
};
export const room = () => {
  return axios.get("/manger/room");
};
export const deletestudent = (id) => {
  return axios.delete("/manger/student/:id=>student_id", {
    params: {
      id
    }
  });
};

export const identityID = () => {
  return axios.get("/user/identity");
};
export const view = () => {
  return axios.get("/user/view_authority");
};

export const api = () => {
  return axios.get("/user/api_authority");
};