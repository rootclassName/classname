import axios from 'axios';
import router from "../router/index.js";
import {Message} from "element-ui";
import resCode from "../config/resCode.js";
axios.defaults.timeout = 2000;

axios.defaults.baseURL = '/api';

axios.interceptors.request.use((config)=>{
    
    if(config.url !=='/user/login'){
        let token=localStorage.getItem('token');
       
        if(token){
           
            config.headers['authorization'] = token
        }else{
            
            router.push('/login')
        }
    }
    return config
},error=>Promise.reject(error))

axios.interceptors.response.use((response)=>{
         if(response.data.code === 1){
            // Message.success({
            //     message:resCode[response.status]
            // })
         }else{
            Message.error({
                message:'登录失败'
            })
         }
         return response
})
export default  axios